from Adafruit_CharLCD import Adafruit_CharLCD
from time import sleep
import Adafruit_DHT

LCD_RS = 4
LCD_EN = 14
LCD_D4 = 18
LCD_D5 = 17
LCD_D6 = 27
LCD_D7 = 22
LCD_COLS = 16
LCD_LINES = 2

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 2

TIME_MEDIUM = 15 # seconds
TIME_SHORT = 3   # seconds

#  Initialize LCD
lcd = Adafruit_CharLCD(rs=LCD_RS, en=LCD_EN,
                       d4=LCD_D4, d5=LCD_D5, d6=LCD_D6, d7=LCD_D7,
                       cols=LCD_COLS, lines=LCD_LINES)

# display message on lcd 16x2
def displayMessage(message, timeSleep=TIME_MEDIUM):
    lcd.clear()
    lcd.message(message)
    sleep(timeSleep)

displayMessage("Welcome!", TIME_SHORT)

try:
    while True:
        # recovering sensor data
        humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
        lcd.clear()

        sensorMessage = "Failed Sensor\n"
        # display formatted data
        if humidity is not None and temperature is not None:
            sensorMessage = "Temp: {0:0.1f}C\nHumi: {1:0.1f}%".format(temperature, humidity)

        displayMessage(sensorMessage)
        
except KeyboardInterrupt:
    print('CTRL-C pressed.  Program exiting...')

finally:
    displayMessage("Bye, bye!", TIME_SHORT)
    lcd.clear()
